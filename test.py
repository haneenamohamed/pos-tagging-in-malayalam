# -*- coding: utf-8 -*-
import nltk

def features(sentence, index):
    return {
        'word': sentence[index],
        'is_first': index == 0,
        'is_last': index == len(sentence) - 1,
        'prefix-1': sentence[index][0],
        'prefix-2': sentence[index][:2],
        'prefix-3': sentence[index][:3],
        'suffix-1': sentence[index][-1],
        'suffix-2': sentence[index][-2:],
        'suffix-3': sentence[index][-3:],
        'prev_word': '' if index == 0 else sentence[index - 1],
        'next_word': '' if index == len(sentence) - 1 else sentence[index + 1],
        'has_hyphen': '-' in sentence[index],
        'is_numeric': sentence[index].isdigit()
    }
 

import pickle
filename = 'model.sav'
clf = pickle.load(open(filename, 'rb'))
 
def pos_tag(sentence):
    tags = clf.predict([features(sentence, index) for index in range(len(sentence))] )#predicts the tag using the features of the input sentence generated.
    print(sentence,tags)
    return sentence , tags
	

from nltk import word_tokenize               #for tokenisation

f=open('input.txt','r',encoding = 'utf-8')   #reads sentence to be POS tagged from filename
text=f.read()
f.close()

sentence, tags = pos_tag(word_tokenize(text))

f=open('pos_out.txt','w',encoding = 'utf-8')     
for i in range(len(sentence)):
	f.write(sentence[i]+' '+tags[i]+'\n')  #writes the words in the sentence with corresponding predicted tags
f.close()
