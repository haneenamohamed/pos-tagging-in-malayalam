# -*- coding: utf-8 -*-
import nltk

tagged_sentences=[[('കോഴിക്കോട്', ' N_NNP'), ('മെഡിക്കൽ', 'N_NN'), ('കോളേജിൽ', 'N_NN'), ('ചികിത്സയിലുള്ള', 'JJ'), ('ഒരു', 'QT_QTC'), ('രോഗിയിൽ', 'N_NN'), ('വെസ്റ്റ്', 'N_NN'), ('നൈൽ', 'N_NN'), ('വൈറസ്', 'N_NN'), ('രോഗം', 'N_NN'), ('റിപ്പോർട്ട്‌', 'PSP'), ('ചെയ്യപ്പെട്ടതിനെ', 'N_NN'), ('തുടർന്ന്‌', 'N_NN'), ('പ്രചരിക്കുന്ന', 'V_VM_VNF'), ('അതിശയോക്തി', 'N_NN'), ('കലർന്ന', 'V_VM_VNF'), ('വാർത്തകൾ', 'N_NN'), ('അപ്പാടെ', 'N_NN'), ('വിശ്വസിക്കേണ്ടതില്ലെന്ന്‌', 'N_NN'), ('പൊതുജനാരോഗ്യ', 'JJ'), ('പ്രവർത്തകനും', 'N_NN'), ('പ്രശസ്‌ത', 'JJ'), ('ന്യൂറോ', 'N_NN'), ('സർജനുമായ', 'JJ'), ('ഡോ.', 'N_NN'), ('ബി', 'N_NNP'), ('ഇക്‌ബാൽ', 'N_NN')],[('കോഴിക്കോട്', ' N_NNP'), ('മെഡിക്കൽ', 'N_NN'), ('കോളേജിൽ', 'N_NN'), ('ചികിത്സയിലുള്ള', 'JJ'), ('ഒരു', 'QT_QTC'), ('രോഗിയിൽ', 'N_NN'), ('വെസ്റ്റ്', 'N_NN'), ('നൈൽ', 'N_NN'), ('വൈറസ്', 'N_NN'), ('രോഗം', 'N_NN'), ('റിപ്പോർട്ട്‌', 'PSP'), ('ചെയ്യപ്പെട്ടതിനെ', 'N_NN'), ('തുടർന്ന്‌', 'N_NN'), ('പ്രചരിക്കുന്ന', 'V_VM_VNF'), ('അതിശയോക്തി', 'N_NN'), ('കലർന്ന', 'V_VM_VNF'), ('വാർത്തകൾ', 'N_NN'), ('അപ്പാടെ', 'N_NN'), ('വിശ്വസിക്കേണ്ടതില്ലെന്ന്‌', 'N_NN'), ('പൊതുജനാരോഗ്യ', 'JJ'), ('പ്രവർത്തകനും', 'N_NN'), ('പ്രശസ്‌ത', 'JJ'), ('ന്യൂറോ', 'N_NN'), ('സർജനുമായ', 'JJ'), ('ഡോ.', 'N_NN'), ('ബി', 'N_NNP'), ('ഇക്‌ബാൽ', 'N_NN')],[('കോഴിക്കോട്', ' N_NNP'), ('മെഡിക്കൽ', 'N_NN'), ('കോളേജിൽ', 'N_NN'), ('ചികിത്സയിലുള്ള', 'JJ'), ('ഒരു', 'QT_QTC'), ('രോഗിയിൽ', 'N_NN'), ('വെസ്റ്റ്', 'N_NN'), ('നൈൽ', 'N_NN'), ('വൈറസ്', 'N_NN'), ('രോഗം', 'N_NN'), ('റിപ്പോർട്ട്‌', 'PSP'), ('ചെയ്യപ്പെട്ടതിനെ', 'N_NN'), ('തുടർന്ന്‌', 'N_NN'), ('പ്രചരിക്കുന്ന', 'V_VM_VNF'), ('അതിശയോക്തി', 'N_NN'), ('കലർന്ന', 'V_VM_VNF'), ('വാർത്തകൾ', 'N_NN'), ('അപ്പാടെ', 'N_NN'), ('വിശ്വസിക്കേണ്ടതില്ലെന്ന്‌', 'N_NN'), ('പൊതുജനാരോഗ്യ', 'JJ'), ('പ്രവർത്തകനും', 'N_NN'), ('പ്രശസ്‌ത', 'JJ'), ('ന്യൂറോ', 'N_NN'), ('സർജനുമായ', 'JJ'), ('ഡോ.', 'N_NN'), ('ബി', 'N_NNP'), ('ഇക്‌ബാൽ', 'N_NN')],[('കോഴിക്കോട്', ' N_NNP'), ('മെഡിക്കൽ', 'N_NN'), ('കോളേജിൽ', 'N_NN'), ('ചികിത്സയിലുള്ള', 'JJ'), ('ഒരു', 'QT_QTC'), ('രോഗിയിൽ', 'N_NN'), ('വെസ്റ്റ്', 'N_NN'), ('നൈൽ', 'N_NN'), ('വൈറസ്', 'N_NN'), ('രോഗം', 'N_NN'), ('റിപ്പോർട്ട്‌', 'PSP'), ('ചെയ്യപ്പെട്ടതിനെ', 'N_NN'), ('തുടർന്ന്‌', 'N_NN'), ('പ്രചരിക്കുന്ന', 'V_VM_VNF'), ('അതിശയോക്തി', 'N_NN'), ('കലർന്ന', 'V_VM_VNF'), ('വാർത്തകൾ', 'N_NN'), ('അപ്പാടെ', 'N_NN'), ('വിശ്വസിക്കേണ്ടതില്ലെന്ന്‌', 'N_NN'), ('പൊതുജനാരോഗ്യ', 'JJ'), ('പ്രവർത്തകനും', 'N_NN'), ('പ്രശസ്‌ത', 'JJ'), ('ന്യൂറോ', 'N_NN'), ('സർജനുമായ', 'JJ'), ('ഡോ.', 'N_NN'), ('ബി', 'N_NNP'), ('ഇക്‌ബാൽ', 'N_NN')],[('കോഴിക്കോട്', ' N_NNP'), ('മെഡിക്കൽ', 'N_NN'), ('കോളേജിൽ', 'N_NN'), ('ചികിത്സയിലുള്ള', 'JJ'), ('ഒരു', 'QT_QTC'), ('രോഗിയിൽ', 'N_NN'), ('വെസ്റ്റ്', 'N_NN'), ('നൈൽ', 'N_NN'), ('വൈറസ്', 'N_NN'), ('രോഗം', 'N_NN'), ('റിപ്പോർട്ട്‌', 'PSP'), ('ചെയ്യപ്പെട്ടതിനെ', 'N_NN'), ('തുടർന്ന്‌', 'N_NN'), ('പ്രചരിക്കുന്ന', 'V_VM_VNF'), ('അതിശയോക്തി', 'N_NN'), ('കലർന്ന', 'V_VM_VNF'), ('വാർത്തകൾ', 'N_NN'), ('അപ്പാടെ', 'N_NN'), ('വിശ്വസിക്കേണ്ടതില്ലെന്ന്‌', 'N_NN'), ('പൊതുജനാരോഗ്യ', 'JJ'), ('പ്രവർത്തകനും', 'N_NN'), ('പ്രശസ്‌ത', 'JJ'), ('ന്യൂറോ', 'N_NN'), ('സർജനുമായ', 'JJ'), ('ഡോ.', 'N_NN'), ('ബി', 'N_NNP'), ('ഇക്‌ബാൽ', 'N_NN')],[('കോഴിക്കോട്', ' N_NNP'), ('മെഡിക്കൽ', 'N_NN'), ('കോളേജിൽ', 'N_NN'), ('ചികിത്സയിലുള്ള', 'JJ'), ('ഒരു', 'QT_QTC'), ('രോഗിയിൽ', 'N_NN'), ('വെസ്റ്റ്', 'N_NN'), ('നൈൽ', 'N_NN'), ('വൈറസ്', 'N_NN'), ('രോഗം', 'N_NN'), ('റിപ്പോർട്ട്‌', 'PSP'), ('ചെയ്യപ്പെട്ടതിനെ', 'N_NN'), ('തുടർന്ന്‌', 'N_NN'), ('പ്രചരിക്കുന്ന', 'V_VM_VNF'), ('അതിശയോക്തി', 'N_NN'), ('കലർന്ന', 'V_VM_VNF'), ('വാർത്തകൾ', 'N_NN'), ('അപ്പാടെ', 'N_NN'), ('വിശ്വസിക്കേണ്ടതില്ലെന്ന്‌', 'N_NN'), ('പൊതുജനാരോഗ്യ', 'JJ'), ('പ്രവർത്തകനും', 'N_NN'), ('പ്രശസ്‌ത', 'JJ'), ('ന്യൂറോ', 'N_NN'), ('സർജനുമായ', 'JJ'), ('ഡോ.', 'N_NN'), ('ബി', 'N_NNP'), ('ഇക്‌ബാൽ', 'N_NN')],[('കോഴിക്കോട്', ' N_NNP'), ('മെഡിക്കൽ', 'N_NN'), ('കോളേജിൽ', 'N_NN'), ('ചികിത്സയിലുള്ള', 'JJ'), ('ഒരു', 'QT_QTC'), ('രോഗിയിൽ', 'N_NN'), ('വെസ്റ്റ്', 'N_NN'), ('നൈൽ', 'N_NN'), ('വൈറസ്', 'N_NN'), ('രോഗം', 'N_NN'), ('റിപ്പോർട്ട്‌', 'PSP'), ('ചെയ്യപ്പെട്ടതിനെ', 'N_NN'), ('തുടർന്ന്‌', 'N_NN'), ('പ്രചരിക്കുന്ന', 'V_VM_VNF'), ('അതിശയോക്തി', 'N_NN'), ('കലർന്ന', 'V_VM_VNF'), ('വാർത്തകൾ', 'N_NN'), ('അപ്പാടെ', 'N_NN'), ('വിശ്വസിക്കേണ്ടതില്ലെന്ന്‌', 'N_NN'), ('പൊതുജനാരോഗ്യ', 'JJ'), ('പ്രവർത്തകനും', 'N_NN'), ('പ്രശസ്‌ത', 'JJ'), ('ന്യൂറോ', 'N_NN'), ('സർജനുമായ', 'JJ'), ('ഡോ.', 'N_NN'), ('ബി', 'N_NNP'), ('ഇക്‌ബാൽ', 'N_NN')],[('കോഴിക്കോട്', ' N_NNP'), ('മെഡിക്കൽ', 'N_NN'), ('കോളേജിൽ', 'N_NN'), ('ചികിത്സയിലുള്ള', 'JJ'), ('ഒരു', 'QT_QTC'), ('രോഗിയിൽ', 'N_NN'), ('വെസ്റ്റ്', 'N_NN'), ('നൈൽ', 'N_NN'), ('വൈറസ്', 'N_NN'), ('രോഗം', 'N_NN'), ('റിപ്പോർട്ട്‌', 'PSP'), ('ചെയ്യപ്പെട്ടതിനെ', 'N_NN'), ('തുടർന്ന്‌', 'N_NN'), ('പ്രചരിക്കുന്ന', 'V_VM_VNF'), ('അതിശയോക്തി', 'N_NN'), ('കലർന്ന', 'V_VM_VNF'), ('വാർത്തകൾ', 'N_NN'), ('അപ്പാടെ', 'N_NN'), ('വിശ്വസിക്കേണ്ടതില്ലെന്ന്‌', 'N_NN'), ('പൊതുജനാരോഗ്യ', 'JJ'), ('പ്രവർത്തകനും', 'N_NN'), ('പ്രശസ്‌ത', 'JJ'), ('ന്യൂറോ', 'N_NN'), ('സർജനുമായ', 'JJ'), ('ഡോ.', 'N_NN'), ('ബി', 'N_NNP'), ('ഇക്‌ബാൽ', 'N_NN')]]

print (tagged_sentences[0])
print ("Tagged sentences: ", len(tagged_sentences))

def features(sentence, index):
    return {
        'word': sentence[index],
        'is_first': index == 0,
        'is_last': index == len(sentence) - 1,
        'prefix-1': sentence[index][0],
        'prefix-2': sentence[index][:2],
        'prefix-3': sentence[index][:3],
        'suffix-1': sentence[index][-1],
        'suffix-2': sentence[index][-2:],
        'suffix-3': sentence[index][-3:],
        'prev_word': '' if index == 0 else sentence[index - 1],
        'next_word': '' if index == len(sentence) - 1 else sentence[index + 1],
        'has_hyphen': '-' in sentence[index],
        'is_numeric': sentence[index].isdigit()
    }
 
def untag(tagged_sentence):
    return [w for w, t in tagged_sentence]  
	
# Split the dataset for training and testing
cutoff = int(.75 * len(tagged_sentences))
training_sentences = tagged_sentences[:cutoff]                        #upto cutoff not including cutoff -75%
test_sentences = tagged_sentences[cutoff:]
 
#print (len(training_sentences))   
#print (len(test_sentences))      
 
def transform_to_dataset(tagged_sentences):
    X, y = [], []
 
    for tagged in tagged_sentences:
        for index in range(len(tagged)):
            X.append(features(untag(tagged), index)) #features
            y.append(tagged[index][1]) #label
 
    return X, y 
X, y = transform_to_dataset(training_sentences)

from sklearn.tree import DecisionTreeClassifier
from sklearn.feature_extraction import DictVectorizer                   #  dict vectoriser ie,num repn
 
from sklearn.pipeline import Pipeline                                   #for parallel processing
 
clf = Pipeline([                                                        #model
    ('vectorizer', DictVectorizer(sparse=False)),                       #vectorise dictionaries
    ('classifier', DecisionTreeClassifier(criterion='entropy'))
])
 
clf.fit(X[:10000], y[:10000])                                           #train with decision tree classifier
 
print ('Training completed')
 
X_test, y_test = transform_to_dataset(test_sentences)
 
print ("Accuracy:", clf.score(X_test, y_test))                          #test the accuracy of the model

import pickle
filename = 'model.sav'                                                  #this trained model is saved into this file
pickle.dump(clf, open(filename, 'wb'))                                  #dump fn is used to save into the file
